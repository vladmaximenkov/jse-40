package ru.vmaksimenkov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.vmaksimenkov.tm.enumerated.Role;
import ru.vmaksimenkov.tm.model.User;

import java.util.List;

public interface IUserService {

    @NotNull
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    User create(@Nullable String login, @Nullable String password, @NotNull Role role);

    boolean existsByEmail(@Nullable String email);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    User findByLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    void setPassword(@NotNull String userId, @Nullable String password);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(@NotNull String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    @Nullable
    List<User> findAll();

    void clear();

    void addAll(@Nullable List<User> list);

    @Nullable
    User findById(@NotNull String id);

}
